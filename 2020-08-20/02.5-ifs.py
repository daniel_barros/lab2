#Pedir un mes al usuario (Enero-Diciembre)
#Mostrar a que estaciones pertenece

diccionario={
    "enero":"verano",
    "febrero":"verano",
    "marzo":"verano",
    "abril":"otoño",
    "mayo":"otoño",
    "junio":"otoño",
    "julio":"invierno",
    "agosto":"invierno",
    "septiembre":"invierno",
    "octubre":"primavera",
    "noviembre":"primavera",
    "diciembre":"primavera",        
}
mes=input("Ingrese el mes: ")
print(diccionario[mes])