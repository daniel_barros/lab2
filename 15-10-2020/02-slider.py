from PyQt5.QtWidgets import QMainWindow,QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("15-10-2020/02-slider.ui",self)
        self.sliderH.valueChanged.connect(self.on_sliderH_changed)
        self.sliderV.valueChanged.connect(self.on_sliderV_changed)
        self.boton.clicked.connect(self.on_clicked)

       
    def on_sliderH_changed(self):        
        self.label01.setText(str(self.sliderH.value()))
        
    def on_sliderV_changed(self):        
        self.label02.setText(str(self.sliderV.value()))
    
    def on_clicked(self):
        total=float(self.label01.text())+float(self.label02.text())
        self.totalSuma.setText("Total: "+str(total))
    
app=QApplication([])

win=MiVentana()
win.show()

app.exec_()