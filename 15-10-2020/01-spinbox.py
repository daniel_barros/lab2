from PyQt5.QtWidgets import QMainWindow,QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("15-10-2020/01-spinbox.ui",self)
        self.cantidadLibros.valueChanged.connect(self.on_cantidad_libros_changed)
        self.cantidadPan.valueChanged.connect(self.on_cantidad_pan_changed)
        self.boton.clicked.connect(self.on_clicked)

       
    def on_cantidad_libros_changed(self):
        precioLibro=float(self.precioLibros.text())
        self.totalLibros.setText(str(precioLibro*self.cantidadLibros.value()))
        
    def on_cantidad_pan_changed(self):
        preciodepan=float(self.precioPan.text())
        self.totalPan.setText(str(preciodepan*self.cantidadPan.value()))
    
    def on_clicked(self):
        total=0
        total=float(self.totalLibros.text())+float(self.totalPan.text())
        self.totalSuma.setText("Total: "+str(total))

app=QApplication([])

win=MiVentana()
win.show()

app.exec_()