from functools import reduce
numeros = [-4, 5, 2, 6, 2, 7, 12, 5, 9]

# Empleando 'reduce' encontrar el producto de cada elemento de la lista

def prod_func(a,b):
    print("a",a)
    print("b",b)
    return a*b

suma= reduce(prod_func,numeros)
print(suma)