from math import pi

class Cilindros:
    #Atributos
    def __init__(self,radio,altura):
        self.radio=radio
        self.altura=altura
        print("Objeto tipo Cilindro")
    
    #Metodos
    def area(self):
        return (2*pi*self.radio*self.altura + 2*pi*self.radio**2)
    
    def volumen(self):
        return ((pi*self.radio**2)*self.altura)


cilindro=Cilindros(3,7)

area_cil=cilindro.area()
vol_cil=cilindro.volumen()

print("Area: ",area_cil)
print("Volumen: ",vol_cil)