#Pedir al usuario el radio
#Calcular diametro, perimetro, area

from mod_circulos import diametroC,perimetroC,areaC

radio=float(input("Ingrese el radio del circulo en cm: "))

print(f"El diametro es: {diametroC(radio)} cm")
print(f"El perimetro es: {perimetroC(radio)} cm")
print(f"El area es: {areaC(radio)} cm2")

#Probando diferentes formas de darle formato a un numero float 
print("El area es: ","{0:.2f}".format(areaC(radio)),"cm2")
print(f"El area es {round(areaC(radio),2)} cm2")

