from persona import Persona

def func_edad(x):
    return x.edad


lista=[Persona("Apellido","Nombre",44, "33333"),Persona("Apellido2","Nombre2",55, "337433"),Persona("Apellido3","Nombre3",52, "5471144")]

lista.sort(key=lambda x:x.edad) #hecho con funciones lambda

#lista.sort(key=func_edad) #hecho con funciones a la antigua
for item in lista:
    print(item)