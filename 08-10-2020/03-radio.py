from PyQt5.QtWidgets import QMainWindow,QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("08-10-2020/03-radio.ui",self)
        self.boton.clicked.connect(self.on_clicked)
        
               
    def on_clicked(self):
        total=0
        if self.jamon.isChecked():
            total=total+20
        if self.huevos.isChecked():
            total=total+10
        if self.tomates.isChecked():
            total=total+50
                   
        self.precio.setText("Precio extras: $"+str(total))
        total=0


app=QApplication([])

win=MiVentana()
win.show()

app.exec_()