from PyQt5.QtWidgets import QMainWindow,QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("08-10-2020/01-radio.ui",self)
        self.opcion1.toggled.connect(self.on_toggled)
        self.opcion2.toggled.connect(self.on_toggled)
        self.opcion3.toggled.connect(self.on_toggled)
        
    def on_toggled(self):
        #print("Cambio de estado...")
        if self.opcion1.isChecked():
            print("Cambio de estado...")
            self.etiqueta.setText("Se eligio la opcion 1")
        elif self.opcion2.isChecked():
            print("Cambio de estado...")
            self.etiqueta.setText("Se eligio la opcion 2")
        elif self.opcion3.isChecked():
            print("Cambio de estado...")
            self.etiqueta.setText("Se eligio la opcion 3")
        else:
            print("Cambio de estado...")
            self.etiqueta.setText("No se eligio opcion")


app=QApplication([])

win=MiVentana()
win.show()

app.exec_()