from PyQt5.QtWidgets import QMainWindow,QApplication,QInputDialog
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("22-10-2020/02-input_dialog.ui",self)     
        self.ingresar.clicked.connect(self.on_ingresar)

    '''    
    def on_ingresar(self):
        texto,ok=QInputDialog.getText(self,'Ingresar','Ingrese un texto')
        if (ok and texto):
            self.entrada.setText(texto)
    '''

    def on_ingresar(self):
        #Enteros
        entero,ok=QInputDialog.getInt(self,'Ingresar','Ingrese un nro entero',value=8,min=0,max=100,step=3)
        if  ok:
            self.entrada.setText(str(entero))
        
        #Decimales
        decimal,ok=QInputDialog.getDouble(self,'Ingresar','Ingrese un decimal',value=8,min=0,max=100,step=1.5)
        if ok:
            self.entrada.setText(str(decimal))
        
        items=['rojo','verde','azul']
        value,ok=QInputDialog.getItem(self,'Ingresar','Ingrese un decimal',)
  
    
app=QApplication([])

win=MiVentana()
win.show()

app.exec_()