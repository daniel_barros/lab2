from PyQt5.QtWidgets import QMainWindow,QApplication,QInputDialog,QMessageBox
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("22-10-2020/03-message_box.ui",self)        
        self.mensaje.clicked.connect(self.on_mensaje)

    def on_mensaje(self):
        msg=QMessageBox()
        msg.setWindowTitle('Titulo del mensaje')
        msg.setText('Este es un mensaje')
        #msg.setIcon(QMessageBox.Critical)
        #msg.setIcon(QMessageBox.Warning)
        #msg.setIcon(QMessageBox.Information)
        msg.setIcon(QMessageBox.Question)

        #msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        respuesta=msg.exec_()
        if respuesta == QMessageBox.Yes:
            print('se eligio si')
        elif respuesta == QMessageBox.No:
            print('se eligio no')
        else: 
            print('se eligio cancelar')

        #msg.exec_()

      
    
app=QApplication([])

win=MiVentana()
win.show()

app.exec_()