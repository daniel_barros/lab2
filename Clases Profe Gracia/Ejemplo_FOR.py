#Ciclo FOR

secuencia=["uno","dos","tres"]
for elemento in secuencia:
    print(elemento)

#Asi es como lo utilizariaos en AnsiC
print("Numeros del 0 al 5")
for i in range(0,6,1):
    print(i)

#en forma decreciente
print("Numeros del 0 al 5")
for i in range(6,0,-1):
    print(i)