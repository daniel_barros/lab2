import random
import string

def generaClaves(numclaves):
	aux=""
	for l in range(numclaves):
		for i in range(0,8):
			if i%2==0:
				if i==0:
					aux+=random.choice(consonantes).upper()
				else:
					aux+=random.choice(consonantes)
			else:
				aux+=random.choice(vocales)
		aux=aux+"_"
		aux=aux+str(random.randint(10,99))    
		aux=aux+","
	aux=aux.split(',')

	for i in range(0,len(aux)-1):
		print(f"{i+1} -> {aux[i]}")




vocales=['a','e','i','o','u']
consonantes=['b','c','d','f','g','h','j','k','l','m','n','ñ','p','q','r','s','t','v','w','x','y','z']

print("Generador de claves")
print("-------------------")

numclaves=int(input("Cuantas claves desea generar: "))

generaClaves(numclaves)



'''
for l in range(numclaves):
	for i in range(0,8):
		if i%2==0:
			if i==0:
				aux+=random.choice(consonantes).upper()
			else:
				aux+=random.choice(consonantes)
		else:
			aux+=random.choice(vocales)
	aux=aux+"_"
	aux=aux+str(random.randint(10,99))    
	aux=aux+","
aux=aux.split(',')

for i in range(0,len(aux)-1):
    print(f"{i+1} -> {aux[i]}")
'''