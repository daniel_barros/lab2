#definir la funcion

def mi_funcion(param1,param2):
    print("Parametro 1 -> "+str(param1))
    print("Parametro 2 -> "+str(param2))

#llamada a la funcion
mi_funcion(45,23)

#-------------------------------------------------------

#defino la segunda funcion
def suma(var1,var2):
    return var1+var2
#llamo a la funcion dentro de print
print("La suma es: "+str(suma(2,3)))

#-------------------------------------------------------

#pasar el retorno de una funcion como parametro
mi_funcion(suma(5,7),10)

