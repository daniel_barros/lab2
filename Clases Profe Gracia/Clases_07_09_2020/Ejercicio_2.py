# Clase: Cancion
# Parametros: titulo, interprete, duracion en segundos, estilo musical
# Metodos:
#   resumen: devuelve titulo e interprete (formato resumido)
#   __str__: devuelve todos los parametros (formato completo)

# Class: Canciones
# Parametro: listado de canciones
# Metodos:
#   Agregar: agrega una cancion
#   listar_por_interprete: Muestra todas las canciones de un interprete(formato resumido)
#   listar_por_estilo: Muestra todas las canciones de un estilo (formato resumido)

#   listar_todas: Muestra todas las canciones (formato completo)

#   quitar: quitar una cancion por titulo e interprete


import datetime

class Lista:
    def __init__(self):
        self.listado_canciones = []
    
    def agregar(self,song):
        if type(song) == Cancion:
            self.listado_canciones.append(song)
            print("Se agregó la canción :"+song.titulo)
        else:
            print("No es una canción")
    
    def listar_todas(self):
        for cancion in self.listado_canciones:
            print(cancion)

class Cancion:
    def __init__(self,titulo,interprete,duracion,genero,anio,sello,album):
        self.titulo = titulo
        self.interprete = interprete
        self.duracion = duracion
        self.genero = genero
        self.anio = anio
        self.sello = sello
        self.album = album
    
    def resumen(self):
        return (self.titulo+" - "+self.interprete)

    def __str__(self):
        return '{0}, {1}, {2}, {3}, {4}, {5}, {6}'.format(self.titulo, self.interprete, datetime.timedelta(minutes=self.duracion), self.genero , self.anio, self.sello, self.album)

cancion1 = Cancion("Enjoy the Silence","Depeche Mode", 2.4, ["Dance Pop","Electro Pop","Synth Pop"],1990,"Mute Records", "Violator")

lista = Lista()
lista.agregar(cancion1)
lista.agregar(cancion1)

lista.listar_todas()

