class Peon:
    def __init__(self,posicionX,posicionY,color):
        self.posicionX=posicionX
        self.posicionY=posicionY
        self.color=color
        self.viva=True

    def __del__(self):
        print("La pieza se fue!")

    def comer(self):
        if(self.color=="negro"):
            self.posicionX=self.posicionX+1
            self.posicionY=self.posicionY+1
        else:
            self.posicionX=self.posicionX-1
            self.posicionY=self.posicionY-1
        
    def avanzar(self):
        if(self.color=="negro"):
            self.posicionY=self.posicionY+1
        else:
            self.posicionY=self.posicionY-1

pieza1=Peon(1,1,"negro")
pieza2=Peon(1,2,"blanco")


       