#Definicion de una clase
class Mario_Bros:
    # sel se refiere al objetico mismo(a si mismo)
    #Para llamar a cada uno de los merodos o el instaciamiento de un objeto debemos
    #pasr por parametros self.
    #Construccion o contruccion del Objeto __init__
    #Destruccion del Objeto __del__
    #Instanciacion - estado incial del objeto
    def __init__(self, color, letra, posicionX, posicionY):
        #Si queremos modificar el estado de un objeto o llamar a un metodo propio de si mismo deberemos
        #hacer uso del self.atributo o self.metodo
        self.color=color
        self.letra=letra
        self.posicionX=posicionX
        self.posicionY=posicionY
        print("Se creo un Mario Bros de Color:"+color+"y con letra: "+letra)

    #Metodos
    def subir(self):
        self.posicionY=self.posicionY + 1        
    def bajar(self):
        self.pasicionY=self.posicionY - 1
    def avanzar(self):
        self.posicionX=self.posicionX + 1
    def retroceder(self):
        self.posicionX=self.posicionX - 1
    #Metodo que espera parametros
    def teletrasportarse(self,posicionX,posicionY):
        print("Bzzzzz!!!!!!")
        self.posicionX=self.posicionX
        self.posicionY=self.posicionY

#instanciar un objeto mediante la clase
personaje=Mario_Bros("rojo","M",0,0)
personaje2=Mario_Bros("verde","L",0,0)

#llamar los metodos definidos de la clase
personaje.avanzar()
personaje.avanzar()
personaje.bajar()
personaje.subir()
personaje.retroceder()

print("Posicion en X: "+str(personaje.posicionX)+" - Posicion en Y: "+str(personaje.posicionY))

#llamado a metodos que esperan parametro
personaje.teletrasportarse(0,0)

print("Posicion en X: "+str(personaje.posicionX)+" - Posicion en Y: "+str(personaje.posicionY))

#HERENCIA SOBREESCRIBIENDO METODOS
#CLASE HIJA 1
class Yoshi(Mario_Bros):
    def __init__(self,color):
        self.color=color
        self.lengua_afuera=False
        self.huevos=0
    
    def sacar_lengua(self):
        self.lengua_afuera=True
    
    def comer(self):
        self.huevos=self.huevos+1


yoshiRosa=Yoshi("rosa")
yoshiRosa.sacar_lengua()
yoshiRosa.comer()
yoshiRosa.comer()

#Herencia de superclase(atributos heredados y metodos heredados)
#Clase Hija 2
class Honguito(Mario_Bros):
    def __init__(self,color,posicionX,posicionY):
        Mario_Bros.__init__(self,posicionX,posicionY)
        self.color=color
        self.scream=False

    def gritar(self):
        self.scream=True
    
honguitoAmarillo=Honguito("amarillo",0,0)
honguitoAmarillo.avanzar()
honguitoAmarillo.retroceder()
honguitoAmarillo.bajar()
honguitoAmarillo.gritar()

print(honguitoAmarillo.posicionX," ",honguitoAmarillo.posicionY)


