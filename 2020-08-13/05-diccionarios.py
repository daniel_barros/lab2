dic={
    "item1":"1er Item",
    "item2":"2do Item"
}

print(dic)

print(dic["item1"])

dic2= {
    "banana": "amarilla",
    "manzana": "roja",
    "pera": "verde"
}

print(dic2["pera"])

print(dic2.items())
print(dic2.values())
print(dic2.keys())

dic2.pop("manzana")

dic2["banana"]="violeta"
#lista los valores dentro del diccionario
print(list(dic2.values()))

print(dic2)
