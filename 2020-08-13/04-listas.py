lista1=[0,5,2,5,-1,10,3,2]

print(lista1)

#agregar elementos uno a la vez
lista1.append(-54)
print(lista1)
#agrega elementos
lista1.extend([45,22])
#agrega elementos en un indice dado
lista1.insert(2,6)
print(lista1)

#ordenar lista
lista1.sort()
print(lista1)
#da vuelta la lista
lista1.reverse()
print(lista1)
#quita un elemento por el indice dado
lista1.pop(8)
print(lista1)
#quita un elemento dado
lista1.remove(3)
print(lista1)


print(len(lista1))
