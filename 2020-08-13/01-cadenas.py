#cadenas de lineas simples
cadena1= 'Una \"cadena" de texto'
cadena2= "Una \'cadena' de texto"

#cadena de varias lineas
cadena3='''
cadena
de texto
en varias
lineas'''
cadena4="""
cadena
de
texto"""

print(cadena1)
print(cadena2)
print(cadena3)
print(cadena4)

#pregunta si "text" esta en cadena1
print('tex' in cadena1)

