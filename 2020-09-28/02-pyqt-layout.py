from PyQt5.QtWidgets import QApplication,QMainWindow,QVBoxLayout,QLabel,QWidget,QHBoxLayout

# Crear Aplicacion
app = QApplication([])

#Crear la ventana principal
ventana= QMainWindow()
ventana.setWindowTitle('Primer programa')

#Agregar componentes
widget = QWidget()
#layout = QVBoxLayout() # Orientacion Vertical
layout = QHBoxLayout() # Orientacion Horizontal
label= QLabel("Hola Mundo!")
label2= QLabel("Chau Mundo!")

layout.addWidget(label)
layout.addWidget(label2)

widget.setLayout(layout)
ventana.setCentralWidget(widget)
ventana.show()

#Ejecutar aplicacion
app.exec_()
