from PyQt5.QtWidgets import QMainWindow,QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("19-10-2020/02-lista.ui",self)        
        self.lista.itemSelectionChanged.connect(self.on_item_changed)  
            
    #cuando se cambia la eleccion
    def on_item_changed(self):
        self.seleccion.clear()
        items=self.lista.selectedItems()
        for item in list(items):
            self.seleccion.addItem(item.text())

    
app=QApplication([])

win=MiVentana()
win.show()

app.exec_()