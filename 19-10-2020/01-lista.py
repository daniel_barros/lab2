from PyQt5.QtWidgets import QMainWindow,QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("19-10-2020/01-lista.ui",self)
        #self.lista.itemClicked.connect(self.on_item_clicked)
        #self.lista.itemSelectionChanged.connect(self.on_item_changed)  
        self.lista.currentItemChanged.connect(self.on_item_changed)

    """
    #cuando se elige el item
    def on_item_clicked(self):
        self.seleccion.setText(self.lista.currentItem().text())
        print("Item Clickeado!")
    
    #cuando se cambia la eleccion
    def on_item_changed(self):
        self.seleccion.setText(self.lista.currentItem().text())
        print("Item Clickeado!")
    """

    def on_item_changed(self, actual, anterior):
        self.seleccion.setText(actual.text())
        if anterior:
            print('anterior: ',anterior.text(), ', actual: ',actual.text())
    

    
app=QApplication([])

win=MiVentana()
win.show()

app.exec_()