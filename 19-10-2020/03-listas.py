from PyQt5.QtWidgets import QMainWindow,QApplication,QInputDialog
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("19-10-2020/03-lista.ui",self)     
        self.agregar.clicked.connect(self.on_clicked)  
        self.editar.clicked.connect(self.on_editar) 
        self.quitar.clicked.connect(self.on_quitar)
        self.quitar_todos.clicked.connect(self.on_quitar_todos)
        self.lista.itemDoubleClicked.connect(self.on_editar)
        self.nombre.returnPressed.connect(self.on_clicked)
        
            
    #cuando se cambia la eleccion
    def on_clicked(self):             
        self.lista.addItem(self.nombre.text())
        self.nombre.clear()
        self.nombre.setFocus()        
    
    def on_quitar(self):
        self.lista.takeItem(self.lista.currentRow())
    
    def on_quitar_todos(self):
        self.lista.clear()
    
    def on_editar(self):
        texto_item=self.lista.currentItem().text()
        new_text, ok = QInputDialog.getText(self,'Editar','Ingrese nuevo nombre',text=texto_item)
        if ok:
            self.lista.currentItem().setText(new_text)



    
app=QApplication([])

win=MiVentana()
win.show()

app.exec_()