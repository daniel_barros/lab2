"""
Pedir al usuario ancho y alto del rectangulo
Crear objeto Rectangulo
Implementar metodo perimetro y area
"""

from rectangulos import Rectangulo

ancho=float(input("Ingrese el ancho del rectangulo: "))
alto=float(input("Ingrese el alto del rectangulo: "))

rectangulo=Rectangulo(ancho,alto)

print("El perimetro del rectangulo es: ",rectangulo.perimetro())
print("El area del rectangulo es: ",rectangulo.area())