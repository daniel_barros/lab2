from math import pi

class Circulo:    
    def __init__(self,radio):        
        self.radio=radio
        print("Objeto circulo creado con radio: ",self.radio)
    
    #Metodos
    def diametroC(self):
        return 2*self.radio

    def perimetroC(self):
        return pi*self.radio*2

    def areaC(self):
        return pi*self.radio**2
        

