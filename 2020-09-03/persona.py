#Crear un objeto persona
#Mostrar el nombre completo

class Persona:
    #Atributos
    def __init__(self,nombre,apellido,edad,dni):
        self.nombre=nombre
        self.apellido=apellido
        self.edad=edad
        self.dni=dni
        print("Objeto persona creado!")

    #Metodos
    def nombrecompleto(self):
        return self.apellido+", "+self.nombre

    def descripcion(self):
        return "{0}, {1}, {2}, {3}".format(self.apellido,self.nombre,self.edad,self.dni)
    
    def __str__(self):
        return "{0}, {1}, {2}, {3}".format(self.apellido,self.nombre,self.edad,self.dni)