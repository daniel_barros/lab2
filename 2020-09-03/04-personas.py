"""
Crear un objeto Personas
Agregar personas al listado
Mostrar el mayor
Mostrar el menor
"""

from persona import Persona
from personas import Personas

persona1=Persona('nombre','apellido',23,'22')
persona2=Persona('nombre2','apellido2',35,'8822')
persona3=Persona('nombre3','apellido3', 10, '5452')

personas=Personas()
personas.agregar(persona1)
personas.agregar(persona2)
personas.agregar(persona3)

print(personas.mayor())
print(personas.menor())

personas.mostrar_todo()

