print("Hola mundo!")

numero_entero=10
numero_decimal=10.5
cadena="aquiva un texto"
boolean=True

# 3ra forma de imprimir variables
print(numero_decimal,numero_entero,cadena,boolean)
#1ra forma de imprimir variables
print("el valor vale {0} {1}".format(numero_entero,numero_decimal))
#2da forma de imprimir variables
print(f"El numero vale {numero_entero} {numero_decimal} {boolean}")

#variables simple y formas de imprimir