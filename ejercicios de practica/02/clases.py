import random
# Clases

class Dado:
    def __init__(self):
        self.cantCaras=0

    def setCaras(self,newCaras):
        self.cantCaras=newCaras

    def getCaras(self):
        return self.cantCaras



class Jugador:
    def __init__(self):
        self.nombre=""
        self.apellido=""
        self.puntaje=0
        self.jugadas=0

    #metodos set
    def setName(self,newName):
        self.nombre=newName
    def setApellido(self,newLastname):
        self.apellido=newLastname

    #metodos get
    def getNombreComp(self):
        nomComp=str(self.nombre)+" "+str(self.apellido)
        return nomComp
    def getNombre(self):
        return self.nombre
    def getApellido(self):
        return self.apellido
    def getPuntaje(self):
        return self.puntaje
    def getScore(self):
        score=str(self.getNombreComp())+" -> "+str(self.puntaje)
        return score

    #metodos otros
    def setTirada(self,carasDado):
        i=0
        cantDados=int(input('Ingrese la cantidad de dados a tirar: '))
        for i in range(cantDados):
            self.puntaje=self.puntaje+random.randint(1,carasDado)

    def setJugadas(self):
        self.jugadas=self.jugadas+1

    def __str__(self):
        return "%i - %s %s" %(self.puntaje,self.nombre,self.apellido)