import clases
import random
import copy
from os import system

opc=99
equipos_futbol=[]
fecha=1

def partido(a,b):
    gol1=random.choice(range(0,5))
    gol2=random.choice(range(0,5))

    if gol1==gol2:
        print(str(equipos_futbol[a].getNombre())+" vs "+str(equipos_futbol[b].getNombre()))
        print("Empate -> "+str(equipos_futbol[a].getNombre())+": "+str(gol1)+" "+str(equipos_futbol[b].getNombre())+": "+str(gol2))
        equipos_futbol[a].setPuntos(1)
        equipos_futbol[b].setPuntos(1)
    if gol1>gol2:
        print(str(equipos_futbol[a].getNombre())+" vs "+str(equipos_futbol[b].getNombre()))
        print("Gano -> "+str(equipos_futbol[a].getNombre())+": "+str(gol1)+" "+str(equipos_futbol[b].getNombre())+": "+str(gol2))
        equipos_futbol[a].setPuntos(3)
        equipos_futbol[b].setPuntos(0)
    if gol1<gol2:
        print(str(equipos_futbol[a].getNombre())+" vs "+str(equipos_futbol[b].getNombre()))
        print("Gano -> "+str(equipos_futbol[b].getNombre())+": "+str(gol2)+" "+str(equipos_futbol[a].getNombre())+": "+str(gol1))
        equipos_futbol[a].setPuntos(0)
        equipos_futbol[b].setPuntos(3)

def fixture(c):    
    if c==1:
        partido(0,1)
        partido(2,3)
    if c==2:
        partido(1,2)
        partido(0,3)
    if c==3:
        partido(0,2)
        partido(3,1)

while opc!=5:
    system("cls")
    print("Torneo de Fútbol")
    print("-----------------------")
    print("1.- Agregar Equipos")
    print("2.- Mostrar Equipos")
    print("3.- Jugar fecha")
    print("4.- Mostrar Tabla")
    print("5.- Salir")

    opc=int(input("Ingrese una opcion: "))

    if opc==1:
        opcequipos=0
        while opcequipos!=2:
            system("cls")
            print("Agregar Equipos")
            print("***************")
            newequipos=clases.Equipos()
            nombre=input("Ingrese el nombre del equipo: ")
            newequipos.setNombre(nombre)
            equipos_futbol.append(newequipos)
            print("Equipo Agregado!!")
            print("Agregar equipo al torneo?")
            opcequipos=int(input("1.- SI o 2.- NO: "))
    
    if opc==2:
        system("cls")
        print("Agregar Equipos")
        print("***************")
        for team in equipos_futbol:
            print(team.getNombre())
        
        input("Ingrese Enter para continuar...")
    
    if opc==3:
        system("cls")
        print("Jugar Fecha")
        print("*******************")
        print("Fecha n°: "+str(fecha))        
        fixture(fecha)
        fecha=fecha+1

        input("Ingrese Enter para continuar...")
    if opc==4:
        system("cls")
        print("Mostrar Tabla")
        print("*******************")
        score=copy.deepcopy(equipos_futbol)
        score.sort(key=lambda x: x.puntos,reverse=True)
        print("Pts  Nombre")
        for team in score:
            print(str(team.getPuntos())+" -> "+team.getNombre())


        input("Ingrese Enter para continuar...")

    if opc==5:
        print("saliendo!!")
        


