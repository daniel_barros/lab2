import random
import string

vocales=['a','e','i','o','u']
consonantes=['b','c','d','f','g','h','j','k','l','m','n','ñ','p','q','r','s','t','v','w','x','y','z']

def generar_clave():
    aux=""
    for i in range(0,8):
        if i%2==0:
            if i==0:
                aux+=random.choice(consonantes).upper()
            else:
                aux+=random.choice(consonantes)
        else:
            aux+=random.choice(vocales)
    aux=aux+"_"
    aux=aux+str(random.randint(10,99))        
    return aux


