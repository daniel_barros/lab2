import funciones
import random

class Roles:
    def __init__(self):
        self.rol_usuario=['Adminstrador','Tecnico','Gerente','Diseñador']
        
    def elegir_rol(self):
        print("\nElegir el rol para asignar...")
        i=0
        for i in range(len(self.rol_usuario)):
            print(str(i)+"->"+self.rol_usuario[i])
        eleccion=int(input("Que rol quiere asignar? "))
        return self.rol_usuario[eleccion]

    def agregar_rol(self,newRol):
        self.rol_usuario.append(newRol)


class Grupos:
    def __init__(self):
        self.grupo_usuario=['Redes','Infraestrucutra y Servicios','Mantenimiento','Cuadrilla']

    def elegir_grupo(self):
        print("\nElegir un grupo para asignar...")
        i=0
        for i in range(len(self.grupo_usuario)):
            print(str(i)+"->"+self.grupo_usuario[i])
        eleccion=int(input("Que grupo quiere asignar? "))
        return self.grupo_usuario[eleccion]
                
    def agregar_grupo(self,newGrupo):
        self.grupo_usuario.append(newGrupo)

    

class Usuarios():
    #atributos
    def __init__(self):        
        self.nombre=""
        self.apellido=""
        self.mail=""
        self.user=""
        self.password=funciones.generar_clave()
        self.password1ra=True
        self.rol=""
        self.grupo=[]
        print("Nuevo Objeto Usuario Creado...")

    #Metodos GET
    def getPassword(self):
        return self.password
    def getUser(self):
        return self.user
    def getRol(self):
        return "Rol: "+self.rol
    def getGrupo(self):
        return self.grupo
    def getInfo(self):
        print("Nombre: "+self.nombre)
        print("Apellido: "+self.apellido)
        print("email: "+self.mail)
        print("User: "+self.user)
        print("Password: "+self.password)
        print("Rol: "+self.rol)
        s=", "
        print("Grupos: "+s.join(self.grupo))

    #metodos SET
    def setRol(self,newrol):
        self.rol=newrol  
    def setNombre(self,newname):
        self.nombre=newname
    def setApellido(self,newlastname):
        self.apellido=newlastname
    def setUser(self,newuser):
        self.user=newuser
    def setMail(self,newmail):
        self.mail=newmail 
    def setGrupo(self,newGrupos):
        if len(self.grupo)==2:
            print("Cupo maximo alcanzado...")
        else:
            self.grupo.append(newGrupos)
    def setPassword(self,newPassword):
        self.password=newPassword
    
    #Metodos otros
    def cambiar_password(self):
        if self.password1ra==False:
            print("Ya cambiaste la password generada por defecto!")
        else:
            newPass=input("Ingresa la nueva clave: ")
            self.password=newPass
            self.password1ra=False
    def quitar_grupo(self):
        if len(self.grupo)==0:
            print("No hay elementos a quitar!!")
        else:
            print("Grupos:")
            print(self.grupo)
            opcpop=int(input("\nIngrese el orden del elemento a quitar: "))-1
            self.grupo.pop(opcpop)
            print("\nGrupos:")
            print(self.grupo)

#-------------------------------------------------------------------------------------
#--------------------------prueba de los objetos--------------------------------------
#-------------------------------------------------------------------------------------
