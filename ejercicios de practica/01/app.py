import clases
import funciones
from os import system

opc=99
users=[]

while opc!=5:
    system("cls")
    print("Creacion de usuarios y cuentas")
    print("******************************")
    print("1.- Creacion de usuarios")
    print("2.- Mostrar usuarios")
    print("3.- Modificar usuarios")
    print("4.- Probar usuarios (Login)")
    print("5.- Salir")

    opc=int(input("Ingrese una opcion: "))

    if opc==1:
        system("cls")
        print("Creacion de Usuarios")
        print("********************")
        newusers=clases.Usuarios()
        nombre=input("\nIngrese el nombre: ")
        newusers.setNombre(nombre)
        apellido=input("Ingrese el apellido: ")
        newusers.setApellido(apellido)
        newusers.setMail(input("Ingrese un mail: "))
        print("Desea cambiar el user por defecto: ?")
        opcUser=int(input("1.- SI o 2.-No "))
        userXdefecto=nombre[0]+apellido       
        if opcUser==1:
            newusers.setUser(input("Ingrese el user: "))
        else:
            newusers.setUser(userXdefecto)
        newroles=clases.Roles()
        newusers.setRol(newroles.elegir_rol())
        newgrupos=clases.Grupos()
        newusers.setGrupo(newgrupos.elegir_grupo())
        print("\nDesea agregar otro grupo?")
        opcGroup=int(input("1.- SI o 2.-No "))
        if opcGroup==1:
            newusers.setGrupo(newgrupos.elegir_grupo())
        print("\nUsuario Creado!")
        users.append(newusers)
        newusers.getInfo()
        x=input("\nPresiones Enter para continuar")               
        
    elif opc==2:
        system("cls")
        print("Mostrar Usuarios")
        print("****************")
        i=0
        if len(users)==0:
            print("No exiten usuarios cargado!!")
        else:
            for i in range(len(users)):
                print("Usuario Nro: "+str(i))
                print(users[i].getInfo())
                print("***************")
        
        x=input("Presiones Enter para continuar")        

    elif opc==3:
        system("cls")
        print("modificar Usuarios")
        print("******************")
        i=0
        if len(users)==0:
            print("No exiten usuarios cargados para modificar!!")
        else:
            for i in range(len(users)):
                print("Usuario Nro: "+str(i))
                print(users[i].getInfo())
                print("***************")
        opcModif=int(input("Que usuario desea modificar? "))
        system("cls")
        print("Selecciono el usuario: "+str(opcModif)+"\n")                
        opcModifAtrib=int(input("Que desas Modificar? \n0.- Nombre\n1.- Apellido\n2.- Mail\n3.- Password\n4.- Rol\n5.- Grupo\n: "))
        if opcModifAtrib==0:
            users[opcModif].setNombre(input("Ingrese el nuevo nombre: "))
        if opcModifAtrib==1:
            users[opcModif].setApellido(input("Ingrese el nuevo apellido"))
        if opcModifAtrib==2:
            users[opcModif].setMail(input("Ingrese el nuevo mail: "))
        if opcModifAtrib==3:
            users[opcModif].setPassword(input("Ingrese la nueva password: "))
        if opcModifAtrib==4:
            newroles=clases.Roles()
            users[opcModif].setRol(newroles.elegir_rol())
        if opcModifAtrib==5:
            if len(users[opcModif].getGrupo())==2:
                users[opcModif].quitar_grupo()
            print("\nIngresar nuevo grupo...\n")
            newgrupos=clases.Grupos()
            users[opcModif].setGrupo(newgrupos.elegir_grupo())

        
    elif opc==4:
        system("cls")
        print("Probar usuarios (Login)")
        print("***********************")
        loginUser=input("Ingrese usuario: ")
        loginPass=input("Ingrese password: ")
        i=0
        for i in range(len(users)):
            if users[i].getUser()==loginUser and users[i].getPassword()==loginPass:
                print("User y Contraseña Correctos!")
                users[i].cambiar_password()
            else:
                print("Usuario o Password Incorrectos!!")
        x=input("Presiones Enter para continuar") 

    elif opc==5:
        print("Saliste de la App...")
    else:
        input("Ingrese una opcion valida!")
        system("cls")

        
        
