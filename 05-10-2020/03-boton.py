from PyQt5.QtWidgets import QMainWindow,QApplication
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("05-10-20/03-boton.ui",self)
        self.boton01.clicked.connect(self.on_clicked_btn01)
        self.boton02.clicked.connect(self.on_clicked_btn02)


    def on_clicked_btn01(self):
        self.boton01.setEnabled(False)
        self.boton02.setEnabled(True)
        
    def on_clicked_btn02(self):
        self.boton01.setEnabled(True)
        self.boton02.setEnabled(False)
        

app=QApplication([])

win=MiVentana()
win.show()

app.exec_()