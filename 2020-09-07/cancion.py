#Clase: Cancion
#Parametros: titulo, interprete, duracion en segundos, estilo musical
#metodos:
#       resumen: devuelve titulo e interprete (Formato Resumido)
#       str: devuelve todos los parametros (Formato completo)

class Cancion:
    #Atributos
    def __init__(self,titulo,interprete,duracion,estilo):
        self.titulo=titulo
        self.interprete=interprete
        self.duracion=duracion
        self.estilo=estilo
        print("Objeto Musica Creado! :)")
    
    #Metodos
    def resumen(self):
        return "Titulo: "+self.titulo+"- Interprete: "+self.interprete
    
    def __str__(self):
        return "Titulo: {0} - Interprete: {1} - Duracion {2} - Estilo: {3}".format(self.titulo,self.interprete,self.duracion,self.estilo)
    
