from cancion import Cancion
from canciones import Canciones

cancion1=Cancion("Bohemian Rhapsody","Queen",360,"Rock")
cancion2=Cancion("Highway to hell","AC/DC",207,"Hard-Rock")
cancion3=Cancion("Smoke on the water","Deep Purple",341,"Hard-Rock")
cancion4=Cancion("Nothing Else Matters","Metallica",389,"Heavy-Metal")
cancion5=Cancion("Around the World","Daft Punk",235,"House")
cancion6=Cancion("Whiskey in the Jar","Metallica",274,"Heavy-Metal")

listaCanciones=Canciones()

listaCanciones.agregar(cancion1)
listaCanciones.agregar(cancion2)
listaCanciones.agregar(cancion3)
listaCanciones.agregar(cancion4)
listaCanciones.agregar(cancion5)
listaCanciones.agregar(cancion6)
print(" ")
listaCanciones.listar_por_interprete("Metallica")
print(" ")
listaCanciones.listar_por_estilo("Hard-Rock")
print(" ")
listaCanciones.listar_todas()
print(" ")
listaCanciones.quitar("Around the World","Daft Punk")
print(" ")
listaCanciones.listar_todas()