"""
Class: Canciones
Parametros: Listadp de canciones
Metodos:
   X Agregar: Agrega una cancion
   X Listar_por_Inteprete: Muestra todas las canciones de un inteprete(Formato resumido)
    Listar_por_estilo: Muestra todas las canciones de un estilo (Formato resumido)
   X listar_todas: muestra todas las canciones(Formato completo)
   X quitar: quitar una cancion por titulo e interprete
"""

class Canciones:
    lista_de_canciones=[]
        
    #Metodos
    def agregar(self,cancion):
        self.lista_de_canciones.append(cancion)
    
    def listar_por_interprete(self,cantante):
        print("Listado por Interprete: "+cantante)
        i=0
        while i<len(self.lista_de_canciones):
            if cantante==self.lista_de_canciones[i].interprete:
                print(self.lista_de_canciones[i].resumen())
            i+=1       
   
    def listar_por_estilo(self,estilo):
        print("Listado por Estilo: "+estilo)
        i=0
        while i<len(self.lista_de_canciones):
            if estilo==self.lista_de_canciones[i].estilo:
                print(self.lista_de_canciones[i].resumen())
            i+=1
        
    def listar_todas(self):
        for canciones in self.lista_de_canciones:
            print(canciones)
    
    def quitar(self,cancion,interprete):
        i=0
        while i<len(self.lista_de_canciones):
            if (cancion==self.lista_de_canciones[i].titulo and interprete==self.lista_de_canciones[i].interprete):
                elemento=i
            i+=1        
        self.lista_de_canciones.pop(elemento)
        print("Se ha quitado a: "+cancion+" de: "+interprete)
        