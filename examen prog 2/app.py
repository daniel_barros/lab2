from os import system
import clases 

opc=99
pelicula=[]
socio=[]
alquileres=[]

while opc!=6:
    system("cls")
    print("\n----VideoClub App----")
    print("******MENU********")
    print("1.- Cargar Pelicula")
    print("2.- Cargar Socio")
    print("3.- Asignar alquiler")
    print("4.- Mostrar peliculas alquiladas")
    print("5.- Mostrar socios con alquiler activo")
    print("6.- Salir")
    opc=int(input("Ingrese una opcion para continuar... "))

    if opc==1:
        system("cls")
        print("Cargar peliculas")
        print("****************")
        nuevapelicula=clases.Peliculas()
        nuevapelicula.setNombre(input("Nombre: "))
        nuevapelicula.setDirector(input("Director: "))
        nuevapelicula.setAnio(input("Año: "))
        pelicula.append(nuevapelicula)
        print("\nPelicula Cargadada!") 
        input("Presiones enter para continuar")       
    elif opc==2:
        system("cls")
        print("Cargar Socio")
        print("************")
        nuevosocio=clases.Socios()
        nuevosocio.setNombreSocio(input("Nombre: "))
        nuevosocio.setApellidoSocio(input("Apellido: "))
        nuevosocio.setDireccionSocio(input("Direccion: "))
        socio.append(nuevosocio)
        print("\nSocio Cargadado!")
        input("Presiones enter para continuar")
    elif opc==3:
        i=0
        opcpeliAlq=0
        system("cls")
        print("Asignar alquiler")
        print("****************")
        if len(pelicula)==0:
            print("No hay peliculas para alquilar, por favor carga primero")
        else:
            for i in range(len(pelicula)):
                print("\nPelicula nro: "+str(i))
                print(pelicula[i].getNombrePeli())
                print("--------------------")
            opcpeliAlq=int(input("Pelicula para alquilar: "))
            
            i=0
            for i in range(len(socio)):
                print("\nSocio nro: "+str(i))   
                print(socio[i].getNombreSocio())
                print("--------------------")
            opcsocioAlq=int(input("Socio que alquilo: "))

            pelicula[opcpeliAlq].estadoAlquilada()
            pelicula[opcpeliAlq].setEstadoAlquiler("SI")
            peliAlq=str(pelicula[opcpeliAlq].getNombrePeli())
            socioAlq=str(socio[opcsocioAlq].getNombreSocio())

            alquiler=socioAlq+" - "+peliAlq
            alquileres.append(alquiler)
            print("\nAlquiler Agregado!")
        input("Presiones enter para continuar")
    elif opc==4:
        i=0
        system("cls")
        print("Mostrar peliculas alquiladas")
        print("****************************")
        if len(alquileres)==0:
            print("No hay peliculas alquiladas")
        else:
            for i in range(len(pelicula)):
                if pelicula[i].getEstadoPeli()=="SI":
                    print(pelicula[i].getNombrePeli())
                    print("--------------------")

        input("Presiones enter para continuar")
    elif opc==5:
        i=0
        system("cls")
        print("Mostrar socios con alquiler activo")
        print("**********************************")
        if len(alquileres)==0:
            print("No hay alquileres asignados!")
        else:            
            for i in range(len(alquileres)):
                print("Alquiner nro: "+str(i)+" "+alquileres[i])
                print("--------------------")
            
        input("Presiones enter para continuar")
    elif opc==6:
        print("Saliste de la App...")    
    else:
        input("\nOpcion no valida! Presiones Enter para Continuar")
        
