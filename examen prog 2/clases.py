
class Peliculas:
    def __init__(self):
        self.nombrePeli=""
        self.directorPeli=""
        self.anioPeli=""
        self.alquilada="NO"

    def setNombre(self,newName):
        self.nombrePeli=newName
    def setDirector(self,newDirector):
        self.directorPeli=newDirector
    def setAnio(self,newAnio):
        self.anioPeli=newAnio
    
    def estadoAlquilada(self):
        if self.alquilada=="SI":
            self.alquilada="NO"
        else:
            self.alquilada="SI"
    def setEstadoAlquiler(self,setalquilo):
        self.alquilada=setalquilo

    def getNombrePeli(self):
        return self.nombrePeli
    def getEstadoPeli(self):
        return self.alquilada
        

class Socios:
    def __init__(self):
        self.nombreSocio=""
        self.apellidoSocio=""
        self.direccionSocio=""
        self.alquilo=False

    def setNombreSocio(self,newNameSocio):
        self.nombreSocio=newNameSocio
    def setApellidoSocio(self,newApellidoSocio):
        self.apellidoSocio=newApellidoSocio
    def setDireccionSocio(self,newDirSocio):
        self.direccionSocio=newDirSocio

    def getNombreSocio(self):
        return self.nombreSocio


